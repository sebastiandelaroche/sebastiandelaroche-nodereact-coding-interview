import React, { useState } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

type OnSearchParams = {
  option: string;
  text: string;
};

type Props = {
  onSearch: (params: OnSearchParams) => void;
};

export const Search: React.FC<Props> = ({ onSearch }) => {
  const [option, setOption] = useState<string>('title');
  const [text, setText] = useState<string>('title');

  const onChangeHandle = (e: any) => {
    setOption(e.target.value);
  };

  const onSearchHandle = () => {
    onSearch({ option, text });
  };

  return (
    <div>
      <Select value={option} onChange={onChangeHandle}>
          <MenuItem value={'title'}>Title</MenuItem>
          <MenuItem value={'gender'}>Gender</MenuItem>
        </Select>
        <TextField value={text} onChange={({ target }) => setText(target?.value)}/>
        <Button variant="contained" color="primary" onClick={onSearchHandle}>
          Search
        </Button>
    </div>
  );
};
