import React from 'react';
import { CircularProgress } from "@mui/material";

export const Loading: React.FC = () => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100vh",
    }}
  >
    <CircularProgress size="60px" />
  </div>
);
