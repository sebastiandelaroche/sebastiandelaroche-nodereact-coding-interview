import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { Loading } from "../components/loading";
import { Search } from "../components/search";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const onSearchHandle = ({ option, text }: any) => {
    console.log(option, text);

  }

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setLoading(false);
    };
    fetchData();
  }, []);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
      {loading && <Loading /> }
      {!loading && (
        <div>
          <Search onSearch={onSearchHandle} />
          {users.length
            ? users.map((user) => {
                return <UserCard key={user.id} {...user} />;
              })
            : null}
        </div>
      )}
      </div>
    </div>
  );
};
